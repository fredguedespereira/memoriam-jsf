package br.edu.ifpb.memoriam.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.memoriam.facade.LoginController;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends GenericBean implements Serializable{
	private static final long serialVersionUID = 1L;

	private String usuario;
	
	private String senha;
	
	private Usuario usuarioLogado;
	
	@Inject
	private LoginController loginController;
	
	public String autenticar() {
		String proxView = null;
		if ((usuarioLogado = loginController.isValido(usuario, senha)) != null) {
			this.setValueOf("#{sessionScope.loginUser}", String.class, usuarioLogado.getEmail());
			proxView = "/contato/consulta?faces-redirect=true";
		} else {
			this.addMessage(FacesMessage.SEVERITY_ERROR, "Login inv�lido.");
		}
		
		return proxView;
	}
	
	public String logout() {
		this.invalidateSession();
		return "/login/login?faces-redirect=true";
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

}
