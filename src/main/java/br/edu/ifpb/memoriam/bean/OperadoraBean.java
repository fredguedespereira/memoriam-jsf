package br.edu.ifpb.memoriam.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.memoriam.facade.OperadoraController;

@Named(value="operadoraBean")
@ViewScoped
public class OperadoraBean extends GenericBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private List<Operadora> operadoras;
	
	@Inject
	private LoginBean loginBean;
	
	private Operadora operadora;
	
	@Inject
	private OperadoraController ctrl;
	
	@PostConstruct
	private void init() {
		Operadora operadora = (Operadora) this.getFlash("operadora");
		if (operadora != null) {
			this.operadora = operadora;
		} else {
			this.operadoras = ctrl.consultar();
			this.operadora  = new Operadora();
		}
		
	}
	
	public String editar(Operadora operadora) {
		this.setFlash("operadora", operadora);
		return "/operadora/cadastro?faces-redirect=true";
	}
	
	public String salvar() {
		String proxView = null;
		try {
			ctrl.cadastrar(operadora);
			this.addSuccessMessage("Operadora salva com sucesso");
			proxView = "/operadora/consulta?faces-redirect=true";
			this.operadora  = new Operadora();
		} catch (PersistenceException e) {
			this.addErrorMessage("Erro ao tentar salvar a operadora");
		}
		return proxView;
	}


	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public List<Operadora> getOperadoras() {
		return operadoras;
	}

	public void setOperadoras(List<Operadora> operadoras) {
		this.operadoras = operadoras;
	}

	public Operadora getOperadora() {
		return operadora;
	}

	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}


}
