package br.edu.ifpb.memoriam.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Usuario;

public class ContatoDAO extends GenericDAO<Contato, Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<Contato> findAllFromUser(Usuario usuario) {
		Query q = entityManager.createQuery("from Contato c where c.usuario = :user");
		q.setParameter("user", usuario);
		return q.getResultList();
	}

	
}
