package br.edu.ifpb.memoriam.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import br.edu.ifpb.memoriam.dao.ContatoDAO;
import br.edu.ifpb.memoriam.dao.Transactional;
import br.edu.ifpb.memoriam.dao.UsuarioDAO;
import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Usuario;

public class ContatoController implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ContatoDAO contatoDAO;
	
	@Inject 
	private UsuarioDAO usuarioDAO;

	@Transactional
	public void cadastrar(Contato contato, Usuario usuario) {
		usuario = usuarioDAO.findByLogin(usuario.getEmail());
		contato.setUsuario(usuario);
		if (contato.getId() == null) {
			contatoDAO.insert(contato);
		} else {
			contatoDAO.update(contato);
		}
	}

	public List<Contato> consultar(Usuario usuario) {
		List<Contato> contatos = contatoDAO.findAllFromUser(usuario);
		return contatos;
	}

	public Contato buscar(Map<String, String[]> parameterMap) throws Exception {
		String[] id = parameterMap.get("id");
		if (id == null)
			throw new Exception("Parametro ID não encontrado.");
		Contato contato = contatoDAO.find(Integer.parseInt(id[0]));
		return contato;
	}

	@Transactional
	public boolean excluir(Contato contato) {
		boolean excluiu = false;
		Contato c = contatoDAO.find(contato.getId());
		contatoDAO.delete(c);
		excluiu = true;
		return excluiu;

	}

}
