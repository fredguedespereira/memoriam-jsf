package br.edu.ifpb.memoriam.facade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.edu.ifpb.memoriam.dao.OperadoraDAO;
import br.edu.ifpb.memoriam.dao.Transactional;
import br.edu.ifpb.memoriam.entity.Operadora;

public class OperadoraController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private OperadoraDAO operadoraDAO;

	public List<Operadora> consultar() {
		List<Operadora> operadoras = operadoraDAO.findAll();
		return operadoras;
	}

	@Transactional
	public Operadora cadastrar(Operadora operadora) {
		if (operadora.getId() == null) {
			operadoraDAO.insert(operadora);
		} else {
			operadoraDAO.update(operadora);
		}
		return operadora;
	}

	@Transactional
	public boolean excluir(Operadora operadora) {
		boolean excluiu = false;
		Operadora c = operadoraDAO.find(operadora.getId());
		operadoraDAO.delete(c);
		return excluiu;
	}

}
