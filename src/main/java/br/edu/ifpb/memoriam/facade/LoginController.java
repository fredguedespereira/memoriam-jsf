package br.edu.ifpb.memoriam.facade;

import java.io.Serializable;

import javax.inject.Inject;

import br.edu.ifpb.memoriam.dao.UsuarioDAO;
import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.memoriam.util.PasswordUtil;

public class LoginController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	public Usuario isValido(String usuario, String senha) {

		Usuario user = usuarioDAO.findByLogin(usuario);
		if (user == null || !senha.equals(user.getSenha())) {
			user = null;
		}

		return user;
	}
}
